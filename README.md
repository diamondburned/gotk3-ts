# gotk3-ts

An attempt to try and make [gotk3](https://gitlab.com/diamondburned/gotk3) thread-safe.

## How it works

As gotk3 is not thread safe, this function attempts to provide wrapper functions for Gtk's thread safe. When `Main()` is called, two goroutines are spawned in the same thread, locked. One calls the actual `gtk.Main()`, the other starts an event loop. All of this is in one thread.

## How to use it

```go
gtkts.Main() // start Gtk main
defer gtkts.Quit()

gtkts.Do(func() {
	label, _ := gtk.LabelNew("test")
	// Do stuff with label, add label to box, ShowAll, etc
})
```
