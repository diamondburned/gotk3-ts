package gtkts

import (
	"runtime"

	"github.com/diamondburned/gotk3/glib"
	"github.com/diamondburned/gotk3/gtk"
)

var evQ chan func()

// BufferSize controls the event queue buffer size.
var BufferSize = 50

// Main initializes Gtk safely while starting the event loop, all in one
// thread to prevent unsafe executions. This function does not block.
func Main() {
	// If the function is already initialized in the past
	if evQ != nil {
		return
	}

	evQ = make(chan func(), BufferSize)

	// Get the old GOMAXPROCS, or the total thread count
	procs := runtime.GOMAXPROCS(0)

	// Set the max thread count to 1
	runtime.GOMAXPROCS(1)

	go func() {
		runtime.LockOSThread()

		for f := range evQ {
			glib.IdleAdd(f)
		}
	}()

	go func() {
		runtime.LockOSThread()
		gtk.Main()
	}()

	// Restore the old GOMAXPROCS count
	runtime.GOMAXPROCS(procs)
}

// Do adds the function to the thread-locked event queue.
func Do(f func()) {
	if f != nil {
		evQ <- f
	}
}

// Quit frees up resources before killing off the Main loops.
func Quit() {
	close(evQ)
	evQ = nil

	gtk.MainQuit()
}
